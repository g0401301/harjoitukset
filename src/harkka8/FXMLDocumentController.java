/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka8;


import static harkka8.BottleDispenser.bd;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;

/**
 *
 * @author Topi
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private Slider moneySlider;
    @FXML
    private TextArea outputTextArea;
    @FXML
    private ComboBox<Bottle> combo;
    @FXML
    private Button addMoneyButton;
    @FXML
    private Button returnMoneyButton;
    @FXML
    private Button buyButton;
    
    
    @FXML
    private void addMoneyAction(ActionEvent event) {
        double temp = moneySlider.getValue();
        bd.addMoney((float) temp);
        
        double temp2 = Math.round(bd.getMoney() * 100.00) / 100.00;
        String temp3 = Double.toString(temp2);
        
        outputTextArea.setText("Rahaa lisätty: " + moneySlider.getValue() + "€\n"
        + "Rahaa sisällä: " + temp3 + "€");
        moneySlider.setValue(0);
    }

    @FXML
    private void returnMoneyAction(ActionEvent event) {
        double temp = Math.round(bd.getMoney() * 100.00) / 100.00;
        String temp2 = Double.toString(temp);
        
        outputTextArea.setText("Rahaa tuli ulos: " + temp2 + "€");
        bd.returnMoney();
         
   }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        BottleDispenser bd = null;
        bd = BottleDispenser.getinstance();
        bd.addBottle(new Bottle("Coca-Cola", 0.5f, 1.2f, 1));
        bd.addBottle(new Bottle("Coca-Cola", 1.5f, 2.5f, 1));
        bd.addBottle(new Bottle("Pepsi", 0.5f, 1.4f, 1));
        bd.addBottle(new Bottle("Pepsi", 1.5f, 2.7f, 1));
        combo.getItems().add(new Bottle("Coca-Cola", 0.5f, 1.2f, 1));
        combo.getItems().add(new Bottle("Coca-Cola", 1.5f, 2.5f,1));
        combo.getItems().add(new Bottle("Pepsi", 0.5f, 1.4f, 1));
        combo.getItems().add(new Bottle("Pepsi", 1.5f, 2.7f, 1));
        
    }   

    @FXML
    private void buyAction(ActionEvent event) throws IOException {
        bd.buyBottle(combo.valueProperty().getValue(), outputTextArea);
    }

    
    
}
