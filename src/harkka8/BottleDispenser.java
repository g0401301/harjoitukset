/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka8;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javafx.scene.control.TextArea;

/**
 *
 * @author Topi
 */
public class BottleDispenser {
    private double money;
    
    private ArrayList<Bottle> bottles = new ArrayList();
    static BottleDispenser bd = null;
    
    private BottleDispenser() {
    money = 0;
    }
    public void addMoney(float x) {
    money = money + x;
    }
    
    public double getMoney() {
        return money;
    }
    
    static public BottleDispenser getinstance() {
        if(bd == null) {
        bd = new BottleDispenser();
        }
        return bd;
    }
    
    public void buyBottle(Bottle b, TextArea area) throws IOException{
        for(int i = 0; i < bottles.size(); i++){
            
            boolean a = b.getName().equals(bottles.get(i).getName());
            boolean c = b.getVolume() == bottles.get(i).getVolume();
            if(a == true && c == true) {
                
                if(b.getPrice() > money) {
                String s = "Et ole syöttänyt tarpeeksi rahaa!";
                area.setText(s);
                }
                else{
                if(bottles.get(i).getAmount() < 1) {
                    String s = "Tuote " + b.getName() + " " + b.getVolume() + " on loppu!";
                    area.setText(s);
                }
                else{
                bottles.get(i).setAmount();
                money = money - b.getPrice();
                
                double temp1 = Math.round(money * 100.00) / 100.00;
            String temp2 = Double.toString(temp1);
                String s = "Ostit tuotteen: " + b.getName() + " " + b.getVolume() + "l\n"
                        + "Rahaa jäljellä: " + temp2 + "€";
                area.setText(s);
                
                String filename = "kuitti.txt";
                FileWriter fw = new FileWriter(filename);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);
                pw.print("KUITTI:\n" + "Ostit tuotteen: " + b.getName() + " " +
                b.getVolume() + "l hinta: " + b.getPrice() + "€");
                pw.close();
                }
                }
                
            }
            
        }
    }
    
    public void addBottle(Bottle b) {
    bottles.add(b);
    }
    
    public void returnMoney() {
        money = 0;
    }
    
    public void printList() {
    int b = bottles.size();
    
    for(int i = 0; i < b; i++) {
    System.out.println(i+1 + ". Nimi: " + bottles.get(i).getName());
    System.out.println("	Koko: " + bottles.get(i).getVolume() + "	Hinta: " + bottles.get(i).getPrice());
    }
    
}
}
