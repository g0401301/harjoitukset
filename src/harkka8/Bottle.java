/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka8;

/**
 *
 * @author Topi
 */
public class Bottle {
    private String name;
    private float volume;
    private float price;
    private int amount;
    
    Bottle(String a, float b, float c, int d) {
    name = a;
    volume = b;
    price = c;
    amount = d;
    }
    
    public String getName() {
    return name;
    }
    
    public int getAmount() {
    return amount;
    }
    
    public float getPrice() {
    return price;
    }
    
    public float getVolume() {
    return volume;
    }
    public String toString() {
     String s = name + "  " + volume + "l  " + price +"€";   
    return s;
    }
    
    public void setAmount() {
    amount = amount - 1;
    }
    
}
